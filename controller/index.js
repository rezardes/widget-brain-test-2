const apiResponse = require('../utils/response');
const { getRandomJokes, removeJokes, fetch10Jokes, freqWordJokes } = require('../jokes')

class controller {
    static getJokes(req, res) {
      var message = "Jokes successfully retrieved!";
      apiResponse.send(res, getRandomJokes(5), message, 200);
    }

    static emptyJokes(req, res) {
      var message = "All jokes successfully deleted!";
      apiResponse.send(res, removeJokes(), message, 200);
    }

    static fetchJokes(req, res) {
       var message = "10 jokes successfully fetched!";
       fetch10Jokes()
       .then(data => {
         apiResponse.send(res, data, message, 201);
       })
       .catch(err => {
          console.log(err);
       })
    }

    static getFreqWordJokes(req, res) {
      var message = "Successfully created frequency words";
      var data = freqWordJokes()
      res.status(200);
      res.json({
         error: false,
         message,
         jokes: data.jokes,
         words: data.words
      })
    }
}

module.exports = controller;