const apiResponse = require('../utils/response')

const notFoundMiddleware = (req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
};

const errorMiddleware = (error, req, res, next) => {
    apiResponse.sendError(res, error);
};

module.exports = {
    notFoundMiddleware,
    errorMiddleware
}