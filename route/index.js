const express = require('express');
const controller = require('../controller');
const router = express.Router();

var jokesApi = '/jokes'

router.get(jokesApi, (req, res) => {
    controller.getJokes(req, res);
});

router.delete(jokesApi, (req, res) => {
    controller.emptyJokes(req, res);
});

router.post(jokesApi, (req, res) => {
    controller.fetchJokes(req, res);
})

router.get(jokesApi + '/freq', (req, res) => {
    controller.getFreqWordJokes(req, res);
})

module.exports = router;