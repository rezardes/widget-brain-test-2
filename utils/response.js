class apiResponse {

    static sendError(res, error) {
        res.status(error.status || 500);
        res.json({
            error: true,
            message: error.message
        });
    }

    static send(res, data, message, status) {
        res.status(status);
        res.json({
            error: false,
            message: message,
            data,
            count: data.length
        });
    }
}

module.exports = apiResponse;