const express = require('express');
const router = require('./route');
const {jokes, initJokes} = require('./jokes');
const { errorMiddleware, notFoundMiddleware } = require('./middleware');
const app = express();

initJokes()
.then(() => {
    //console.log(jokes);
})
.catch(err => {
    console.log(err);
})

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', router);

app.use(notFoundMiddleware);
app.use(errorMiddleware);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));